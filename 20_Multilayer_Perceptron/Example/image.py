import matplotlib.pyplot as plt
import numpy as np

plt.figure(figsize=(12, 6))


def show_imgs(i, j, data):
    imgs = []
    y = []
    for index in range(i, j + 1):
        img = data[0][index:index + 1]
        y.append(data[1][index:index + 1][0])
        img = np.reshape(img, (28, 28))
        if index == i:
            imgs = img
        else:
            imgs = np.concatenate((imgs, img), axis=1)

    plt.imshow(imgs, cmap='gray')
    print(y)
    plt.show()


def show_img(img):
    img = np.reshape(img, (28, 28))
    plt.imshow(img, cmap='gray')
    plt.show()
